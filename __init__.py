from flask import Flask, render_template, flash, request, url_for, \
    redirect, session, send_file, send_from_directory, jsonify

from passlib.hash import sha256_crypt
import gc
import os
import pygal

from functools import wraps

from utils.content_mgmt import content
from utils.forms import RegistrationForm
from utils.dbconnect import connection

from flask_mail import Mail, Message


app = Flask(
    __name__, instance_path='/home/watson/Documents/CodigosPrueba/web-banana/protected')
app.config.update(
    DEBUG=True,
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USERNAME='',
    MAIL_PASSWORD=''
)
mail = Mail(app)

app.secret_key = "super secret key"

TOPIC_DICT = content()


@app.route('/')
def homepage():
    return render_template("main.html")


@app.route('/dashboard/', methods=['GET', 'POST'])
def dashboard():
    try:
        try:
            client_name, settings, tracking, rank = user_info()
            if len(tracking) < 10:
                tracking = '/introduction-python/'
            gc.collect()

            if client_name == "Guest":
                flash("welcome Guest, feel free to browse content. Progress \
                    tracking is only available for users.")

            update_user_tracking()

            completed_percentages = topic_completion_percent()
            return render_template("dashboard.html", topic_dict=TOPIC_DICT)

        except Exception as e:
            return((str(e), "Please report errors to Ana Banana"))
    except Exception as e:
        return((str(e), "Please report errors to Ana Banana"))


@app.route('/login/', methods=['GET', 'POST'])
def login_page():
    error = ''
    try:
        conn, cur = connection()
        if request.method == "POST":
            data = cur.execute("SELECT * FROM banana_user WHERE username = %s",
                               (request.form['username'],))

            data = cur.fetchone()[2]

            if sha256_crypt.verify(request.form['password'], data):
                session['logged_in'] = True
                session['username'] = request.form['username']

                flash("You are logged in :)")
                return redirect(url_for('dashboard'))
            else:
                error = "Invalid credentials. Try again."

        gc.collect()
        return render_template("login.html", error=error)

    except Exception:
        # flash(e)
        error = "Invalid credentials, try again."
        return render_template("login.html", error=error)
    return render_template("login.html")


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('login_page'))
    return wrap


@app.route('/logout/')
@login_required
def logout():
    session.clear()
    flash("You have been logged out!")
    gc.collect()
    return redirect(url_for('dashboard'))


@app.route('/register/', methods=['GET', 'POST'])
def register_page():
    try:
        form = RegistrationForm(request.form)
        if request.method == 'POST' and form.validate():
            username = form.username.data
            email = form.email.data
            password = sha256_crypt.encrypt((form.password.data))

            conn, cur = connection()

            cur.execute(
                "SELECT * FROM banana_user WHERE username = %s", (username,))
            used_user = cur.rowcount

            cur.execute(
                "SELECT * FROM banana_user WHERE email = %s", (email,))
            used_email = cur.rowcount

            if used_user > 0:
                flash("Username already taken.")
                return render_template("register.html", form=form)
            elif used_email > 0:
                flash("Email already taken.")
                return render_template("register.html", form=form)
            else:
                cur.execute("INSERT INTO banana_user (username, password, email, tracking) VALUES (%s, %s, %s, %s)",
                            (username, password, email, "/introduction-python/"))

                conn.commit()
                flash("Thanks for registering!")
                cur.close()
                conn.close()

                gc.collect()

                session['loggeg_in'] = True
                session['username'] = username

                return redirect(url_for('dashboard'))
        return render_template("register.html", form=form)

    except Exception as e:
        return(str(e))


@app.route('/include_example/')
def include_example():
    try:
        replies = {
            'Jack': 'Cool post',
            'Jane': '+1'
        }
        return render_template('includes_tutorial.html', replies=replies)
    except Exception as e:
        return(str(e))


def special_requirement(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        try:
            if 'anab' == session['username']:
                return f(*args, **kwargs)
            else:
                return redirect(url_for('dashboard'))
        except:
            return redirect(url_for('dashboard'))
    return wrap


@app.route('/secret/<path:filename>/')
@special_requirement
def protected(filename):
    try:
        return send_from_directory(os.path.join(app.instance_path, ''), filename)
    except Exception:
        return redirect(url_for('main'))


@app.route('/background_process')
def background_process():
    try:
        lang = request.args.get('proglang', 0, type=str)
        if lang.lower() == 'python':
            return jsonify(result='You are wise')
        else:
            return jsonify(result='Try again.')
    except Exception as e:
        return str(e)


@app.route('/interactive/')
def interactive():
    try:
        return render_template('interactive.html')
    except Exception as e:
        return(str(e))


@app.route('/pygal_example/')
def pygal_example():
    try:
        graph = pygal.Line()
        graph.title = '% Change Coolness of programming languages over time.'
        graph.x_labels = ['2011', '2012', '2013', '2014', '2015', '2016']
        graph.add('Python',  [15, 31, 89, 200, 356, 900])
        graph.add('Java',    [15, 45, 76, 80,  91,  95])
        graph.add('C++',     [5,  51, 54, 102, 150, 201])
        graph.add('All others combined!',  [5, 15, 21, 55, 92, 105])
        graph_data = graph.render_data_uri()
        return render_template('graphing.html', graph_data=graph_data)
    except Exception as e:
        return(str(e))


@app.route('/jinjaman/')
def jinjaman():
    try:
        data = [15, '15', 'Python is good', 'Python, Jva, php, SQL, C++',
                '<p><strong>Hey there!</strong></p>']
        return render_template('jinja_templating.html', data=data)
    except Exception as e:
        return(str(e))


@app.route('/converters/')
@app.route('/converters/<string:thread>/<int:page>/')
def converterexample(thread='test', page=1):
    try:
        gc.collect()
        return render_template('converterexample.html', thread=thread, page=page)
    except Exception as e:
        return(str(e))


@app.route('/send-mail/')
def send_mail():
    try:
        msg = Message("Send Mail Tutorial1",
                      sender=app.config.get('MAIL_USERNAME'),
                      recipients=[""])
        msg.body = "Have you heard the good word of Python?"

        mail.send(msg)
        return 'Mail sent'

    except Exception as e:
        return str(e)


@app.route('/return-file/')
def return_file():
    return send_file('/home/watson/Documents/CodigosPrueba/web-banana/static/images/bananalogo.png')


@app.route('/file-downloads/')
def file_downloads():
    return render_template('downloads.html')


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")


@app.errorhandler(405)
def method_not_found(e):
    return render_template("405.html")


@app.errorhandler(500)
def internal_server_error(e):
    return render_template("500.html", error=str(e))


def user_info():
    settings, tracking, rank = '', '', ''
    try:
        client_name = session['username']
        guest = False
    except:
        guest = True
        client_name = 'Guest'

    if not guest:
        try:
            conn, cur = connection()
            cur.execute("SELECT * FROM user WHERE username = %s",
                        (client_name,))

            data = cur.fetchone()
            settings = data[4]
            tracking = data[5]
            rank = data[6]
        except Exception as e:
            pass
    else:
        settings = [0, 0]
        tracking = [0, 0]
        rank = [0, 0]
    return client_name, settings, tracking, rank


def update_user_tracking():
    try:
        completed = str(request.args['completed'])

        if completed in str(TOPIC_DICT.values()):
            client_name, settings, tracking, rank = user_info()

            if tracking == None:
                tracking = completed
            else:
                if completed not in tracking:
                    tracking = tracking + ',' + completed

            conn, cur = connection()
            cur.execute("UPDATE user SET tracking = %s WHERE username = %s",
                        (tracking, client_name))
            conn.commit()
            cur.close()
            conn.close()
            client_name, settings, tracking, rank = user_info()
        else:
            pass

    except Exception as e:
        pass


def topic_completion_percent():
    try:
        client_name, settings, tracking, rank = user_info()

        try:
            tracking = tracking.split(',')
        except:
            pass

        if tracking == None:
            tracking = []

        completed_percentages = {}

        for each_topic in TOPIC_DICT:
            total = 0
            total_complete = 0

            for each in TOPIC_DICT[each_topic]:
                total += 1
                for done in tracking:
                    if done == each[1]:
                        total_complete += 1

            percent_complete = int((total_complete*100)/total)
            completed_percentages[each_topic] = percent_complete

        return completed_percentages
    except:
        for each_topic in TOPIC_DICT:
            total = 0
            total_complete = 0

            completed_percentages[each_topic] = 0.0

        return completed_percentages

    pass


if __name__ == "__main__":
    app.run(debug=True)
