# importing libraries
import os
from flask import Flask
from flask_mail import Mail, Message

app = Flask(__name__)
mail = Mail(app)  # instantiate the mail class

# configuration of mail
mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": 'anitapinto325@gmail.com',
    "MAIL_PASSWORD": '*****',
    "MAIL_SUPPRESS_SEND": False,
    "TESTING": False,
    "DEBUG": True
}
app.config.update(mail_settings)
mail = Mail(app)

# message object mapped to a particular URL ‘/’
@app.route("/")
def index():
    msg = Message(
        'Hello',
        sender=app.config.get('MAIL_USERNAME'),
        recipients=['anitapinto325@gmail.com'],
        body="This is a test email I sent with Gmail and Python!"
    )
    mail.send(msg)
    return 'Sent'


if __name__ == '__main__':
    app.run(debug=True)
