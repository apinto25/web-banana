from wtforms import Form, TextField, PasswordField, BooleanField, validators


class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=20)])
    email = TextField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [validators.Required(),
                                          validators.EqualTo('confirm', message='Password must match')])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField(
        'I accept the <a href="/tos/">Terms of Services</a> and the <a href="/privacy/">Privacy Notice</a>.',
        [validators.Required()])
