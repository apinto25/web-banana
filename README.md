# Web Banana
Flask web app.

## Usage

Create a python virtual environment and activate it:

    $ virtualenv <venv_name>
    $ source <venv_name>/bin/activate

Install requirements.txt

    $ pip install -r requirements.txt
    